﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Chili.Data.DbObjects;


namespace Chili.Data
{
    public class ApplicationContext: DbContext
    {
        public ApplicationContext()
        {
            
        }

        public DbSet<AddressDbObject> Address { get; set; }
        public DbSet<CategoryDbObject> Category { get; set; }
        public DbSet<MenuDbObject> Menu { get; set; }
        public DbSet<OrderDbObject> Order { get; set; }
        public DbSet<OrderDetailDbObject> OrderDetail { get; set; }
        public DbSet<ProductDbObject> Product { get; set; }
        public DbSet<PurchaserDbObject> Purchaser { get; set; }
        public DbSet<SizeDbObject> Size { get; set; }
        public DbSet<UserDbObject> User { get; set; }
        public DbSet<UserRoleDbObject> UserRole { get; set; }

    }
}
