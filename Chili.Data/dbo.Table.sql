﻿CREATE TABLE [dbo].[Table]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NULL, 
    [AddedDate] DATETIME NULL, 
    [ModifiedDate] DATETIME NULL
)
