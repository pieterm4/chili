﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chili.Data.DbObjects
{
    [Table("Purchaser")]
    public class PurchaserDbObject: BaseEntity
    {
        public string Name { get; set; }
        public string Street { get; set; }
        public AddressDbObject Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}
