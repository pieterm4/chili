﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chili.Data.DbObjects
{
    [Table("User")]
    public class UserDbObject: BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserRoleDbObject Role { get; set; }
    }
}
