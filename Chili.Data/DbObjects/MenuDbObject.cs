﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chili.Data.DbObjects
{
    [Table("Menu")]
    public class MenuDbObject: BaseEntity
    {
        public ProductDbObject Product { get; set; }
        public SizeDbObject Size { get; set; }
        public decimal Price { get; set; }
    }
}
