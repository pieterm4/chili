﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chili.Data.DbObjects
{
    [Table("OrderDetail")]
    public class OrderDetailDbObject: BaseEntity
    {
        public MenuDbObject Menu { get; set; }
        public int Amount { get; set; }
        public decimal Total { get; set; }
    }
}
