﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chili.Data.DbObjects
{
    [Table("Address")]
    public class AddressDbObject :BaseEntity
    {
        public List<string> Name { get; set; }
    }
}
