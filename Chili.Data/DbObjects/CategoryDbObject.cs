﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chili.Data.DbObjects
{
    [Table("Category")]
    public class CategoryDbObject: BaseEntity
    {
        public string Name { get; set; }
    }
}
