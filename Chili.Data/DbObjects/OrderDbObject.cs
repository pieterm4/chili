﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chili.Data.DbObjects
{
    [Table("Order")]
    public class OrderDbObject: BaseEntity
    {
        public UserDbObject User { get; set; }
        public PurchaserDbObject Purchaser { get; set; }
        public OrderDetailDbObject OrderDetail { get; set; }
    }
}
