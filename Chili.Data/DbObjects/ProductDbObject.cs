﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chili.Data.DbObjects
{
    [Table("Product")]
    public class ProductDbObject: BaseEntity
    {
        public string Name { get; set; }
        public CategoryDbObject Category { get; set; }
    }
}
