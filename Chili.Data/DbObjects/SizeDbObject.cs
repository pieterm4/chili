﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chili.Data.DbObjects
{
    [Table("Size")]
    public class SizeDbObject: BaseEntity
    {
        public string Name { get; set; }
    }
}
