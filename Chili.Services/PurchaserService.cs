﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data;
using Chili.Data.DbObjects;
using Chili.Services.Interfaces;

namespace Chili.Services
{
    public class PurchaserService: Repository<PurchaserDbObject>, IPurchaserService
    {
        public PurchaserService(ApplicationContext context) : base(context)
        {
        }

        public IQueryable<PurchaserDbObject> GetList()
        {
            return base.GetAll();
        }

        public async Task AddEntity(PurchaserDbObject purchaser)
        {
            if (purchaser == null)
            {
                throw new ArgumentNullException(nameof(purchaser));
            }
            await base.AddAsync(purchaser);
        }

        public async Task UpdateEntity(PurchaserDbObject purchaser)
        {
            if (purchaser == null)
            {
                throw new ArgumentNullException(nameof(purchaser));
            }
            await base.UpdateAsync(purchaser);
        }

        public async Task DeleteEntity(PurchaserDbObject purchaser)
        {
            if (purchaser == null)
            {
                throw new ArgumentNullException(nameof(purchaser));
            }
            await base.DeleteAsync(purchaser);
        }
    }
}
