﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data;
using Chili.Data.DbObjects;
using Chili.Services.Interfaces;

namespace Chili.Services
{
    public class OrderDetailService: Repository<OrderDetailDbObject>, IOrderDetailService
    {
        public OrderDetailService(ApplicationContext context) : base(context)
        {
        }

        public IQueryable<OrderDetailDbObject> GetList()
        {
            return base.GetAll();
        }

        public async Task AddEntity(OrderDetailDbObject orderDetail)
        {
            if (orderDetail == null)
            {
                throw new ArgumentNullException(nameof(orderDetail));
            }
            await base.AddAsync(orderDetail);
        }

        public async Task UpdateEntity(OrderDetailDbObject orderDetail)
        {
            if (orderDetail == null)
            {
                throw new ArgumentNullException(nameof(orderDetail));
            }
            await base.UpdateAsync(orderDetail);
        }

        public async Task DeleteEntity(OrderDetailDbObject orderDetail)
        {
            if (orderDetail == null)
            {
                throw new ArgumentNullException(nameof(orderDetail));
            }
            await base.DeleteAsync(orderDetail);
        }
    }
}
