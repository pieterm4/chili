﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Chili.Data;
using Chili.Data.DbObjects;
using Chili.Services.Interfaces;

namespace Chili.Services
{
    public class AddressService: Repository<AddressDbObject>, IAddressService
    {
        public AddressService(ApplicationContext context) : base(context)
        {
        }

        public IQueryable<AddressDbObject> GetList()
        {
            return base.GetAll();
        }

        public async Task AddEntity(AddressDbObject address)
        {
            if (address == null)
            {
                throw new ArgumentNullException(nameof(address));
            }
            await base.AddAsync(address);
        }

        public async Task UpdateEntity(AddressDbObject address)
        {
            if (address == null)
            {
                throw new ArgumentNullException(nameof(address));
            }
            await base.UpdateAsync(address);
        }

        public async Task DeleteEntity(AddressDbObject address)
        {
            if (address == null)
            {
                throw new ArgumentNullException(nameof(address));
            }
            await base.DeleteAsync(address);
        }


       
    }
}
