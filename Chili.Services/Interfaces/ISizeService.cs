﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Chili.Data.DbObjects;

namespace Chili.Services.Interfaces
{
    public interface ISizeService
    {
        IQueryable<SizeDbObject> GetList();
        Task AddEntity(SizeDbObject size);
        Task UpdateEntity(SizeDbObject size);
        Task DeleteEntity(SizeDbObject size);
    }
}
