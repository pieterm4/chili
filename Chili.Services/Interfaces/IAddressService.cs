﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Chili.Data.DbObjects;

namespace Chili.Services.Interfaces
{
    public interface IAddressService
    {
        IQueryable<AddressDbObject> GetList();
        Task AddEntity(AddressDbObject address);
        Task UpdateEntity(AddressDbObject address);
        Task DeleteEntity(AddressDbObject address);
    }
}
