﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data.DbObjects;

namespace Chili.Services.Interfaces
{
    public interface IOrderService
    {
        IQueryable<OrderDbObject> GetList();
        Task AddEntity(OrderDbObject order);
        Task UpdateEntity(OrderDbObject order);
        Task DeleteEntity(OrderDbObject order);
    }
}
