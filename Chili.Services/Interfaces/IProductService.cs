﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data.DbObjects;

namespace Chili.Services.Interfaces
{
    public interface IProductService
    {
        IQueryable<ProductDbObject> GetList();
        Task AddEntity(ProductDbObject product);
        Task UpdateEntity(ProductDbObject product);
        Task DeleteEntity(ProductDbObject product);
    }
}
