﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data.DbObjects;

namespace Chili.Services.Interfaces
{
    public interface IPurchaserService
    {
        IQueryable<PurchaserDbObject> GetList();
        Task AddEntity(PurchaserDbObject purchaser);
        Task UpdateEntity(PurchaserDbObject purchaser);
        Task DeleteEntity(PurchaserDbObject purchaser);
    }
}
