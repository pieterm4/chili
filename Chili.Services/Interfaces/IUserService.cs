﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data.DbObjects;

namespace Chili.Services.Interfaces
{
    public interface IUserService
    {
        IQueryable<UserDbObject> GetList();
        Task AddEntity(UserDbObject user);
        Task UpdateEntity(UserDbObject user);
        Task DeleteEntity(UserDbObject user);
    }
}
