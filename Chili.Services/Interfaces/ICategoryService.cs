﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Chili.Data.DbObjects;

namespace Chili.Services.Interfaces
{
    public interface ICategoryService
    {
        IQueryable<CategoryDbObject> GetList();
        Task AddEntity(CategoryDbObject category);
        Task UpdateEntity(CategoryDbObject category);
        Task DeleteEntity(CategoryDbObject category);
    }
}
