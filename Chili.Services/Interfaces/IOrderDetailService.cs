﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data.DbObjects;

namespace Chili.Services.Interfaces
{
    public interface IOrderDetailService
    {
        IQueryable<OrderDetailDbObject> GetList();
        Task AddEntity(OrderDetailDbObject orderDetail);
        Task UpdateEntity(OrderDetailDbObject orderDetail);
        Task DeleteEntity(OrderDetailDbObject orderDetail);
    }
}
