﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data.DbObjects;

namespace Chili.Services.Interfaces
{
    public interface IUserRoleService
    {
        IQueryable<UserRoleDbObject> GetList();
        Task AddEntity(UserRoleDbObject userRole);
        Task UpdateEntity(UserRoleDbObject userRole);
        Task DeleteEntity(UserRoleDbObject userRole);
    }
}
