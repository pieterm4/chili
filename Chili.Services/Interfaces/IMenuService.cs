﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Chili.Data.DbObjects;

namespace Chili.Services.Interfaces
{
    public interface IMenuService
    {
        IQueryable<MenuDbObject> GetList();
        Task AddEntity(MenuDbObject menu);
        Task UpdateEntity(MenuDbObject menu);
        Task DeleteEntity(MenuDbObject menu);
    }
}
