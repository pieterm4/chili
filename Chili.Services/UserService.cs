﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data;
using Chili.Data.DbObjects;
using Chili.Services.Interfaces;

namespace Chili.Services
{
    public class UserService: Repository<UserDbObject>, IUserService
    {
        public UserService(ApplicationContext context) : base(context)
        {
        }

        public IQueryable<UserDbObject> GetList()
        {
            return base.GetAll();
        }

        public async Task AddEntity(UserDbObject user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            await base.AddAsync(user);
        }

        public async Task UpdateEntity(UserDbObject user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            await base.UpdateAsync(user);
        }

        public async Task DeleteEntity(UserDbObject user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            await base.DeleteAsync(user);
        }
    }
}
