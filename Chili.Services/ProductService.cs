﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data;
using Chili.Data.DbObjects;
using Chili.Services.Interfaces;

namespace Chili.Services
{
    public class ProductService: Repository<ProductDbObject>, IProductService
    {
        public ProductService(ApplicationContext context) : base(context)
        {
        }

        public IQueryable<ProductDbObject> GetList()
        {
            return base.GetAll();
        }

        public async Task AddEntity(ProductDbObject product)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            await base.AddAsync(product);
        }

        public async Task UpdateEntity(ProductDbObject product)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }
           await base.UpdateAsync(product);
        }

        public async Task DeleteEntity(ProductDbObject product)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            await base.DeleteAsync(product);
        }
    }
}
