﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data;
using Chili.Data.DbObjects;
using Chili.Services.Interfaces;

namespace Chili.Services
{
    public class OrderService: Repository<OrderDbObject>, IOrderService
    {
        public OrderService(ApplicationContext context) : base(context)
        {
        }

        public IQueryable<OrderDbObject> GetList()
        {
            return base.GetAll();
        }

        public async Task AddEntity(OrderDbObject order)
        {
            if(order == null)
            {
                throw new ArgumentNullException(nameof(order));
            }
            await base.AddAsync(order);
        }

        public async Task UpdateEntity(OrderDbObject order)
        {
            if (order == null)
            {
                throw new ArgumentNullException(nameof(order));
            }
            await base.UpdateAsync(order);
        }

        public async Task DeleteEntity(OrderDbObject order)
        {
            if (order == null)
            {
                throw new ArgumentNullException(nameof(order));
            }
            await base.DeleteAsync(order);
        }
    }
}
