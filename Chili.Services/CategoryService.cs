﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Chili.Data;
using Chili.Data.DbObjects;
using Chili.Services.Interfaces;

namespace Chili.Services
{
    public class CategoryService: Repository<CategoryDbObject>, ICategoryService
    {
        public CategoryService(ApplicationContext context) : base(context)
        {
        }

        public IQueryable<CategoryDbObject> GetList()
        {
            return base.GetAll();
        }

        public async Task AddEntity(CategoryDbObject category)
        {
            if (category == null)
            {
                throw new ArgumentNullException(nameof(category));
            }
            await base.AddAsync(category);
        }

        public async Task UpdateEntity(CategoryDbObject category)
        {
            if (category == null)
            {
                throw new ArgumentNullException(nameof(category));
            }
            await base.UpdateAsync(category);
        }

        public async Task DeleteEntity(CategoryDbObject category)
        {
            if (category == null)
            {
                throw new ArgumentNullException(nameof(category));
            }
            await base.DeleteAsync(category);
        }
    }
}
