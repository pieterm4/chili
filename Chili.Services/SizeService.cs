﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Chili.Data;
using Chili.Data.DbObjects;
using Chili.Services.Interfaces;

namespace Chili.Services
{
    public class SizeService: Repository<SizeDbObject>, ISizeService
    {
        public SizeService(ApplicationContext context) : base(context)
        {
        }

        public IQueryable<SizeDbObject> GetList()
        {
            return base.GetAll();
        }

        public async Task AddEntity(SizeDbObject size)
        {
            if (size == null)
            {
                throw new ArgumentNullException(nameof(size));
            }
            await base.AddAsync(size);
        }

        public async Task UpdateEntity(SizeDbObject size)
        {
            if (size == null)
            {
                throw new ArgumentNullException(nameof(size));
            }
            await base.UpdateAsync(size);
        }

        public async Task DeleteEntity(SizeDbObject size)
        {
            if (size == null)
            {
                throw new ArgumentNullException(nameof(size));
            }
            await base.DeleteAsync(size);
        }
    }
}
