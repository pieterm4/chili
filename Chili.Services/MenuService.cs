﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data;
using Chili.Data.DbObjects;
using Chili.Services.Interfaces;

namespace Chili.Services
{
    public class MenuService:Repository<MenuDbObject>, IMenuService
    {
        public MenuService(ApplicationContext context) : base(context)
        {
        }

        public IQueryable<MenuDbObject> GetList()
        {
            return base.GetAll();
        }

        public async Task AddEntity(MenuDbObject menu)
        {
            if (menu == null)
            {
                throw new ArgumentNullException(nameof(menu));
            }
            await base.AddAsync(menu);
        }

        public async Task UpdateEntity(MenuDbObject menu)
        {
            if (menu == null)
            {
                throw new ArgumentNullException(nameof(menu));
            }
            await base.UpdateAsync(menu);
        }

        public async Task DeleteEntity(MenuDbObject menu)
        {
            if (menu == null)
            {
                throw new ArgumentNullException(nameof(menu));
            }
            await base.DeleteAsync(menu);
        }

        
    }
}
