﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chili.Data;
using Chili.Data.DbObjects;
using Chili.Services.Interfaces;

namespace Chili.Services
{
    public class UserRoleService: Repository<UserRoleDbObject>, IUserRoleService
    {
        public UserRoleService(ApplicationContext context) : base(context)
        {
        }

        public IQueryable<UserRoleDbObject> GetList()
        {
            return base.GetAll();
        }

        public async Task AddEntity(UserRoleDbObject userRole)
        {
            if (userRole == null)
            {
                throw new ArgumentNullException(nameof(userRole));
            }
            await base.AddAsync(userRole);
        }

        public async Task UpdateEntity(UserRoleDbObject userRole)
        {
            if (userRole == null)
            {
                throw new ArgumentNullException(nameof(userRole));
            }
            await base.UpdateAsync(userRole);
        }

        public async Task DeleteEntity(UserRoleDbObject userRole)
        {
            if (userRole == null)
            {
                throw new ArgumentNullException(nameof(userRole));
            }
            await base.DeleteAsync(userRole);
        }
    }
}
