﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Chili.Data;
using Chili.Data.DbObjects;
using Chili.Services;

namespace Chili
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {



        private SizeDbObject size;

        public MainWindow()
        {
            InitializeComponent();
        }


        private async void button_Click(object sender, RoutedEventArgs e)
        {
            //SizeDbObject size = new SizeDbObject()
            //{
            //    Id = 0,
            //    AddedDate = DateTime.Now,
            //    ModifiedDate = DateTime.Now,
            //    Name = "Dupa"
            //};


            //ApplicationContext context = new ApplicationContext();
            //SizeService sizeService = new SizeService(context);
            //await sizeService.InsertAsync(size);


            //Dodanie

            ApplicationContext context = new ApplicationContext();
            CategoryService categoryservice = new CategoryService(context);

            List<CategoryDbObject> cat = new List<CategoryDbObject>(categoryservice.GetList());
            

            ProductDbObject product = new ProductDbObject()
            {
                Id = 0,
                AddedDate = DateTime.Now,
                Category = cat[0],
                    
                ModifiedDate = DateTime.Now,
                Name = "Margerita2"
            };
            ProductDbObject product1 = new ProductDbObject()
            {
                
                AddedDate = DateTime.Now,
                Category = cat[5],
                    
                ModifiedDate = DateTime.Now,
                Name = "KebabGigant2"
            };
            
            
            ProductService service = new ProductService(context);
            await service.AddAsync(product1);

            string txt = "Dodano!";
            MessageBox.Show(txt);




        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //Wydruk 
            ApplicationContext context = new ApplicationContext();
            ProductService service = new ProductService(context);
            List<ProductDbObject> lista = new List<ProductDbObject>(service.GetList());
            string wydruk = String.Empty;
            foreach (var l in lista)
            {
                wydruk += l.Name + "\n";

            }

            MessageBox.Show(wydruk);

        }

        private async void button2_Click(object sender, RoutedEventArgs e)
        {
            ApplicationContext context = new ApplicationContext();
            ProductService service = new ProductService(context);

            var lista = new List<ProductDbObject>(service.GetList());
            var pro = lista.Where(c => c.Id == 0);
            await service.DeleteAsync(lista[0]);
            MessageBox.Show("Usunieto!");
        }
    }
}
